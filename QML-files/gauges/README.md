# Gauges
This style places a gauge on each polygon that shows an indication of the value in the table.  The exact value is also shown below the gauge.

<table><tr><td><a href="https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gauges/gauges.qml?inline=false"><img src="../../Example_images/gauges.png"></a></td></tr></table> 

[Download the QML file for this Geometry Generator Style](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/gauges/gauges.qml?inline=false)
