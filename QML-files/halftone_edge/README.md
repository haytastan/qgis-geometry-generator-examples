# Halftone edge
With this style you can add points to the inside of the edge of polygons that resemble a halftone pattern. Version 1 of this style uses Geometry Generators multiple times to achieve the result.  version 2 of the style uses only one Geometry Generator and uses Line Offset to achieve a similar result.  The results of both versions are slightly different, so you use the version that suits your layer best.

<table><tr><td><img src="../../Example_images/halftone_edge.png"></td></tr></table> 

[Download the QML file for this Geometry Generator Style version 1](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/halftone_edge/halftone_edge_v1.qml?inline=false)

[Download the QML file for this Geometry Generator Style version 2](https://gitlab.com/GIS-projects/qgis-geometry-generator-examples/raw/master/QML-files/halftone_edge/halftone_edge_v2.qml?inline=false)
