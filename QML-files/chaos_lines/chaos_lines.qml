<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis version="3.10.2-A Coruña" styleCategories="Symbology">
  <renderer-v2 enableorderby="0" type="singleSymbol" symbollevels="0" forceraster="0">
    <symbols>
      <symbol alpha="1" name="0" type="marker" clip_to_extent="1" force_rhr="0">
        <layer class="SimpleMarker" locked="0" pass="0" enabled="1">
          <prop v="0" k="angle"/>
          <prop v="166,6,22,255" k="color"/>
          <prop v="1" k="horizontal_anchor_point"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="circle" k="name"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0" k="outline_width"/>
          <prop v="3x:0,0,0,0,0,0" k="outline_width_map_unit_scale"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="diameter" k="scale_method"/>
          <prop v="2" k="size"/>
          <prop v="3x:0,0,0,0,0,0" k="size_map_unit_scale"/>
          <prop v="MM" k="size_unit"/>
          <prop v="1" k="vertical_anchor_point"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
        </layer>
        <layer class="GeometryGenerator" locked="0" pass="0" enabled="1">
          <prop v="Line" k="SymbolType"/>
          <prop v="  with_variable( 'start_x', x( @map_extent_center ), with_variable( 'start_y',  y( @map_extent_center ), &#xd;&#xa;smooth( make_line(&#xd;&#xa;&#x9;$geometry, &#xd;&#xa;           make_point(&#xd;&#xa;&#x9;&#x9;4*randf(0,1)*(x($geometry)-@start_x)/5+@start_x,&#xd;&#xa;&#x9;&#x9;4*randf(0,1)*(y($geometry)-@start_y)/5+@start_y&#xd;&#xa;&#x9;&#x9;),&#xd;&#xa;           make_point(&#xd;&#xa;&#x9;&#x9;3*randf(0,1)*(x($geometry)-@start_x)/5+@start_x,&#xd;&#xa;&#x9;&#x9;3*randf(0,1)*(y($geometry)-@start_y)/5+@start_y&#xd;&#xa;&#x9;&#x9;),&#xd;&#xa;           make_point(&#xd;&#xa;&#x9;&#x9;2*randf(0,1)*(x($geometry)-@start_x)/5+@start_x,&#xd;&#xa;&#x9;&#x9;2*randf(0,1)*(y($geometry)-@start_y)/5+@start_y&#xd;&#xa;&#x9;&#x9;),&#xd;&#xa;&#x9;   make_point(&#xd;&#xa;&#x9;&#x9;1*randf(0,1)*(x($geometry)-@start_x)/5+@start_x,&#xd;&#xa;&#x9;&#x9;1*randf(0,1)*(y($geometry)-@start_y)/5+@start_y&#xd;&#xa;&#x9;&#x9;),&#xd;&#xa;           make_point(@start_x, @start_y)),&#xd;&#xa;&#x9;5,0.3,0.1,360)&#xd;&#xa;&#x9;)&#xd;&#xa;&#x9;)" k="geometryModifier"/>
          <data_defined_properties>
            <Option type="Map">
              <Option name="name" type="QString" value=""/>
              <Option name="properties"/>
              <Option name="type" type="QString" value="collection"/>
            </Option>
          </data_defined_properties>
          <symbol alpha="1" name="@0@1" type="line" clip_to_extent="1" force_rhr="0">
            <layer class="SimpleLine" locked="0" pass="0" enabled="1">
              <prop v="round" k="capstyle"/>
              <prop v="5;2" k="customdash"/>
              <prop v="3x:0,0,0,0,0,0" k="customdash_map_unit_scale"/>
              <prop v="MM" k="customdash_unit"/>
              <prop v="0" k="draw_inside_polygon"/>
              <prop v="round" k="joinstyle"/>
              <prop v="35,35,35,255" k="line_color"/>
              <prop v="solid" k="line_style"/>
              <prop v="0.3" k="line_width"/>
              <prop v="MM" k="line_width_unit"/>
              <prop v="0" k="offset"/>
              <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
              <prop v="MM" k="offset_unit"/>
              <prop v="0" k="ring_filter"/>
              <prop v="0" k="use_custom_dash"/>
              <prop v="3x:0,0,0,0,0,0" k="width_map_unit_scale"/>
              <data_defined_properties>
                <Option type="Map">
                  <Option name="name" type="QString" value=""/>
                  <Option name="properties"/>
                  <Option name="type" type="QString" value="collection"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerGeometryType>0</layerGeometryType>
</qgis>
